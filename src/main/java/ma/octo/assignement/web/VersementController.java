package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersement;
import ma.octo.assignement.service.VersementService;


@RestController
@RequestMapping("/versements")
public class VersementController {

	@Autowired
	private IVersement versementService = new VersementService();
	
	@GetMapping("/lister_versements")
	List<VersementDto> loadAll(){
		return versementService.loadAll();
	}
	@PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<VersementDto> createTransaction(@RequestBody VersementDto versementDto) throws  CompteNonExistantException, TransactionException {
    	versementService.createTransaction(versementDto);
        return ResponseEntity.ok(versementDto);
    }
}
