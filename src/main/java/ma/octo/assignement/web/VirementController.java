package ma.octo.assignement.web;


import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVirement;
import ma.octo.assignement.service.VirementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/virements")

class VirementController {

	@Autowired
	private IVirement virementService = new VirementService();
    
	@GetMapping("/lister_virements")
	List<VirementDto> loadAll() {
        return virementService.loadAll();
    }
    /*List<Virement> loadAll() {
        return virementService.loadAll();
    }*/

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<VirementDto> createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        virementService.createTransaction(virementDto);
        return ResponseEntity.ok(virementDto);
    }
}
