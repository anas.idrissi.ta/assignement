package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.service.DashboardService;
import ma.octo.assignement.service.IDashboard;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {
	
	@Autowired
	private IDashboard dashboardService = new DashboardService();
	
	@GetMapping("/lister_virements")
    List<VirementDto> loadAllVirement() {
        return dashboardService.loadAllVirement();
    }
	
	@GetMapping("/lister_versements")
    List<VersementDto> loadAllVersement() {
        return dashboardService.loadAllVersement();
    }

    @GetMapping("/lister_comptes")
    List<CompteDto> loadAllCompte() {
        return dashboardService.loadAllCompte();
    }

    @GetMapping("/lister_utilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        return dashboardService.loadAllUtilisateur();
    }

}
