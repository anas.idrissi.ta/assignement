package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {

	
	public static VersementDto map(Versement versement) {
		VersementDto versementDto = new VersementDto();
		versementDto.setNomEmetteur(versement.getNom_prenom_emetteur());
    	versementDto.setDate(versement.getDateExecution());
    	versementDto.setMotif(versement.getMotifVersement());
    	versementDto.setRibBeneficiaire(versement.getCompteBeneficiaire().getRib());
    	versementDto.setMontantVersement(versement.getMontantVersement());
        return versementDto;
	}
	
	//Construire VersementEntity depuis Versementdto
	public static Versement mapToEntity(VersementDto versementDto) {
		Versement versementEntity = new Versement();
		versementEntity.setDateExecution(versementDto.getDate());
		versementEntity.setMontantVersement(versementDto.getMontantVersement());
		versementEntity.setMotifVersement(versementDto.getMotif());
		return versementEntity;
	}
}
