package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    //private static VirementDto virementDto;

    public static VirementDto map(Virement virement) {
    	
    	VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        //Ajout montant
        virementDto.setMontantVirement(virement.getMontantVirement());
        //Ajout nrCompteBeneficiaire
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        return virementDto;

    }
    
    public static Virement mapToEntity(VirementDto virementDto) {
    	Virement virementEntity = new Virement();
    	virementEntity.setDateExecution(virementDto.getDate());
    	virementEntity.setMontantVirement(virementDto.getMontantVirement());;
    	virementEntity.setMotifVirement(virementDto.getMotif());
    	return virementEntity;
    }
}
