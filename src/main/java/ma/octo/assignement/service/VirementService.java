package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.utils.Messages;

@Service
@Transactional

public class VirementService extends TransactionService implements IVirement{

	@Autowired
	private CompteRepository compteRepository;
	@Autowired 
	private VirementRepository virementRepository;
	@Autowired
	private AudiService serviceAudit;
	
    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);
    //public static final int MONTANT_MAXIMAL = 10000;
	
    // loadAll doit retourner VirmentDto
	@Override
	public List<VirementDto> loadAll() {
		List<Virement> all = virementRepository.findAll();
		List<VirementDto> allDto = new ArrayList<VirementDto>();
		if(all.isEmpty())
			return null;
		for(Virement v: all) {
			allDto.add(VirementMapper.map(v));
			System.out.println(allDto.get(0).getMotif());
		}
		return allDto;
	}
    /*@Override
    public List<Virement> loadAll() {
		List<Virement> all = virementRepository.findAll();
		if(CollectionUtils.isEmpty(all))
			return null;
		
		return all;
	}*/

	@Override
	public void createTransaction(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

		Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteNotExist(compteEmetteur) || compteNotExist(compteBeneficiaire)) {
            //System.out.println("Compte Non existant");
        	LOGGER.error(Messages.COMPTE_NOT_EXIST);

        	throw new CompteNonExistantException(Messages.COMPTE_NOT_EXIST);
        }
        
        
        checkMontantTransaction(virementDto.getMontantVirement());

        /*
        if (virementDto.getMontantVirement().equals(null) || virementDto.getMontantVirement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
        */

        //Le motif ne doit pas etre vide
        if (isMotifVide(virementDto.getMotif())) {
            //System.out.println("Motif vide");
        	LOGGER.error(Messages.MOTIF_VIDE);
        	throw new TransactionException(Messages.MOTIF_VIDE);
        }

        if (compteEmetteur.getSolde().compareTo(virementDto.getMontantVirement()) < 0) {
            LOGGER.error(Messages.SOLDE_INSUFFISANT);
            // Ajout de throw new
            throw new SoldeDisponibleInsuffisantException(Messages.SOLDE_INSUFFISANT);
        }
        //Repetition de code
        /*if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }*/

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        //compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(virementDto.getMontantVirement()));

        compteRepository.save(compteBeneficiaire);

        //la fonction mapToEntity cree un Virement depuis virementDto
        Virement virement = VirementMapper.mapToEntity(virementDto);
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        
        //virementRepository.save(virement);
        this.save(virement);

        /*serviceAudit.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                        .toString());*/
        serviceAudit.auditVirement(virementDto.getMessage());

	}

	@Override
	public void save(Virement virement) {
		virementRepository.save(virement);
		
	}

}
