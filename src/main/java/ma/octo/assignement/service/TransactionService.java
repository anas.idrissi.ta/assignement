package ma.octo.assignement.service;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.utils.Constants;
import ma.octo.assignement.utils.Messages;


public abstract class TransactionService {

    Logger LOGGER = LoggerFactory.getLogger(TransactionService.class);
    
    protected void checkMontantTransaction(BigDecimal montant) throws TransactionException
    {
    	if (isMontantNull(montant)) {
    		LOGGER.error(Messages.MONTANT_VIDE);
            throw new TransactionException(Messages.MONTANT_VIDE);
        } else if (isMontantInfMin(montant)) {
        	LOGGER.error(Messages.MONTANT_NON_ATTEINT);
            throw new TransactionException(Messages.MONTANT_NON_ATTEINT);
        } else if (isMontanSupMax(montant)) {
        	LOGGER.error(Messages.MONTANT_DEPASSE);
            throw new TransactionException(Messages.MONTANT_DEPASSE);
        }
    }
    
    protected boolean isMontantNull(BigDecimal montant) {
    	
    	return(montant.equals(null) || montant.compareTo(new BigDecimal("0")) == 0) ? true: false;
    }
    
    protected boolean isMontantInfMin(BigDecimal montant) {
    	//return (montant.intValue() < Constants.MONTANT_MINIMAL) ? true: false;
    	return (montant.compareTo(new BigDecimal(Constants.MONTANT_MINIMAL)) < 0) ? true: false;
    }
    
    protected boolean isMontanSupMax(BigDecimal montant) {
    	return(montant.compareTo(new BigDecimal(Constants.MONTANT_MAXIMAL)) > 0)? true: false;
    }
    
    protected boolean isMotifVide(String motif) {
    	return (motif.length() == 0) ? true: false;
    }
    
    protected boolean compteNotExist(Compte compte)
    {
    	return (compte == null) ? true: false;
    }

}
