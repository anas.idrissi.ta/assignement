package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;

public interface IDashboard {

	public List<VirementDto> loadAllVirement();
	public List<CompteDto> loadAllCompte();
	public List<UtilisateurDto> loadAllUtilisateur();
	public List<VersementDto> loadAllVersement();
}
