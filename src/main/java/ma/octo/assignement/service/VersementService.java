package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.utils.Messages;

@Service
@Transactional
public class VersementService extends TransactionService implements IVersement{

    @Autowired
	private AudiService serviceAudit;
    @Autowired
	private CompteRepository compteRepository;
	@Autowired
	private VersementRepository versementRepository;
    
	
	@Override
	public List<VersementDto> loadAll() {
		List<Versement> all = versementRepository.findAll();
		List<VersementDto> allDto = new ArrayList<VersementDto>();
		if (all.isEmpty()) {
            return null;
        }
		for(Versement v : all) {
			allDto.add(VersementMapper.map(v));
		}
		return allDto;
	}

	
	@Override
	public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
	
		Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRibBeneficiaire());
        
		if (compteNotExist(compteBeneficiaire)) {
        	LOGGER.error(Messages.COMPTE_NOT_EXIST);
        	throw new CompteNonExistantException(Messages.COMPTE_NOT_EXIST);
        }
        
        if (versementDto.getNomEmetteur().length() == 0 || versementDto.getNomEmetteur() == null) {
        	LOGGER.error(Messages.NOM_EMETTEUR_VIDE);
            throw new TransactionException(Messages.NOM_EMETTEUR_VIDE);
        }

        checkMontantTransaction(versementDto.getMontantVersement());

        if (isMotifVide(versementDto.getMotif())) {
        	LOGGER.error(Messages.MOTIF_VIDE);
        	throw new TransactionException(Messages.MOTIF_VIDE);
        }

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(versementDto.getMontantVersement()));
        compteRepository.save(compteBeneficiaire);
        
        Versement versement = VersementMapper.mapToEntity(versementDto);
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setNom_prenom_emetteur(versementDto.getNomEmetteur());

        this.save(versement);

        serviceAudit.auditVersement(versementDto.getMessage());
		
	}

	@Override
	public void save(Versement versement) {
		versementRepository.save(versement);
		
	}

	
}
