package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional
public class DashboardService implements IDashboard{

	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VersementRepository versementRepository;
	@Autowired 
	private VirementRepository virementRepository;
	
	@Override
	public List<VirementDto> loadAllVirement() {
		List<Virement> all = virementRepository.findAll();
		List<VirementDto> allDto = new ArrayList<VirementDto>();
		if(CollectionUtils.isEmpty(all))
			return null;
		for(Virement v: all) {
			allDto.add(VirementMapper.map(v));
		}
		return allDto;
	}

	@Override
	public List<CompteDto> loadAllCompte() {
		List<Compte> all = compteRepository.findAll();
		List<CompteDto> allDto = new ArrayList<CompteDto>();
		if(all.isEmpty())
			return null;
		for(Compte c: all) {
			allDto.add(CompteMapper.map(c));
		}
		return allDto;
	}

	@Override
	public List<UtilisateurDto> loadAllUtilisateur() {
		List<Utilisateur> all = utilisateurRepository.findAll();
		List<UtilisateurDto> allDto = new ArrayList<UtilisateurDto>();
		if(all.isEmpty())
			return null;
		for(Utilisateur u: all) {
			allDto.add(UtilisateurMapper.map(u));
		}
		return allDto;
	}

	@Override
	public List<VersementDto> loadAllVersement() {
		List<Versement> all = versementRepository.findAll();
		List<VersementDto> allDto = new ArrayList<VersementDto>();
		if (all.isEmpty()) {
            return null;
        }
		for(Versement v : all) {
			allDto.add(VersementMapper.map(v));
		}
		return allDto;
	}

}
