package ma.octo.assignement.utils;

public class Messages {

	public static final String MOTIF_VIDE = "Motif vide";
    public static final String COMPTE_NOT_EXIST = "Compte non existant";
    public static final String SOLDE_INSUFFISANT = "Solde insuffisant pour l'utilisateur";
	public static final String NOM_EMETTEUR_VIDE = "Nom emetteur vide";
	public static final String MONTANT_VIDE = "Montant vide";
	public static final String MONTANT_NON_ATTEINT = "Montant minimal de virement non atteint";
	public static final String MONTANT_DEPASSE = "Montant maximal de virement dépassé";



	

}

