package ma.octo.assignement.dto;

import java.math.BigDecimal;

public class CompteDto {

	private String nrCompte;
	private String rib;
	private BigDecimal solde;
	private UtilisateurDto utilisateur;
	public String getNrCompte() {
		return nrCompte;
	}
	public void setNrCompte(String nrCompte) {
		this.nrCompte = nrCompte;
	}
	public String getRib() {
		return rib;
	}
	public void setRib(String rib) {
		this.rib = rib;
	}
	public BigDecimal getSolde() {
		return solde;
	}
	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}
	public UtilisateurDto getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(UtilisateurDto utilisateur) {
		this.utilisateur = utilisateur;
	}
	
}
