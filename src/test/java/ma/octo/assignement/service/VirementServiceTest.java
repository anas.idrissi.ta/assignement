package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@Transactional
class VirementServiceTest {
	
	  @Autowired
	  private CompteRepository compteRepository;
	  
	  @Autowired
	  private VirementRepository virementRepository;
	  
	  @Autowired
	  VirementService virementService;
	  

	private VirementDto createVirementTest(Compte compteEmetteur, Compte compteBeneficiaire) {
		    VirementDto v = new VirementDto();
			v.setMontantVirement(new BigDecimal("200"));
			v.setNrCompteEmetteur(compteEmetteur.getNrCompte());
			v.setNrCompteBeneficiaire(compteBeneficiaire.getNrCompte());
			v.setDate(new Date());
			v.setMotif("test");
			return v;
	  }
	
	@Test
	void createTransaction() throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
		
		  int size = virementRepository.findAll().size();
		  VirementDto v = createVirementTest(compteRepository.findAll().get(0), compteRepository.findAll().get(1));
		  virementService.createTransaction(v);
		  assertThat(virementRepository.findAll().size()).isGreaterThan(size);	
	}
	

}
