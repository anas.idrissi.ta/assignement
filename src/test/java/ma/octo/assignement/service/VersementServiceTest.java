package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.utils.Constants;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@Transactional
class VersementServiceTest {
	
	  @Autowired
	  private CompteRepository compteRepository;
	  
	  @Autowired
	  private VersementRepository versementRepository;
	  
	  @Autowired
	  VersementService versementService;
	  

	private VersementDto createVersementTest(Compte compte) {
		  VersementDto v = new VersementDto();
			v.setMontantVersement(new BigDecimal("200"));
			v.setRibBeneficiaire(compte.getRib());
			v.setNomEmetteur("anas");
			v.setDate(new Date());
			v.setMotif("test");
			return v;
	  }
	
	
	
	@Test
	void montantNullTest() {
		boolean test = versementService.isMontantNull(new BigDecimal("0"));
		assertThat(test).isTrue();
	}
	
	@Test
	void montantInfMinTest() {
		boolean test = versementService.isMontantInfMin(new BigDecimal("5"));
		assertThat(test).isTrue();
	}
	
	@Test
	void montantSupMaxTest() {
		boolean test = versementService.isMontanSupMax(new BigDecimal("10000000000000"));
		assertThat(test).isTrue();
	}
	
	@Test
	void montantVideTest() {
		boolean test = versementService.isMotifVide("");
		assertThat(test).isTrue();
	}
	
	@Test
	void createTransaction() throws CompteNonExistantException, TransactionException {
		
		  int size = versementRepository.findAll().size();
		  VersementDto v = createVersementTest(compteRepository.findAll().get(0));
		  versementService.createTransaction(v);
		  assertThat(versementRepository.findAll().size()).isGreaterThan(size);	
	}
	

}
